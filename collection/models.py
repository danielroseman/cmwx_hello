from django.contrib.auth.models import User
from django.db import models
from django import forms

class Person(models.Model):
	team = models.ForeignKey('Team', blank=True, null=True)
	user = models.OneToOneField(User, blank=True, null=True)

	def __str__(self):
		return "{} ({})".format(self.user.username, self.team.name)


class Team(models.Model):
	name = models.CharField(max_length=255, unique=True)
	description = models.TextField(blank=True)
	slug = models.SlugField(unique=True)

	def game_count(self):
		return self.games_as_challenger.count() + self.games_as_opponent.count()

	def __str__(self):
		return self.name

class Game(models.Model):
	creator = models.ForeignKey(Person)
	challenger = models.ForeignKey(Team, related_name="games_as_challenger")
	opponent = models.ForeignKey(Team, related_name="games_as_opponent")
	winner = models.ForeignKey(Team, related_name="games_as_winner", null=True, blank=True)
	loser = models.ForeignKey(Team, related_name="games_as_loser", null=True, blank=True)

	def __str__(self):
		return "{} vs {}".format(self.challenger.name, self.opponent.name)
