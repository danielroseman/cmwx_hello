# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('collection', '0029_auto_20150911_1944'),
    ]

    operations = [
        migrations.AddField(
            model_name='thing',
            name='new_team_name',
            field=models.CharField(max_length=200, default=1),
            preserve_default=False,
        ),
    ]
