# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('collection', '0036_convert_teams'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='person',
            name='description',
        ),
        migrations.RemoveField(
            model_name='person',
            name='slug',
        ),
        migrations.RemoveField(
            model_name='person',
            name='team_name',
        ),
    ]
