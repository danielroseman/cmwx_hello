# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('collection', '0002_thing_user'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='thing',
            name='user',
        ),
    ]
