# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('collection', '0006_auto_20150910_0149'),
    ]

    operations = [
        migrations.RenameField(
            model_name='thing',
            old_name='user',
            new_name='owner',
        ),
        migrations.AddField(
            model_name='thing',
            name='teams',
            field=models.ForeignKey(blank=True, to='collection.Thing', null=True),
        ),
        migrations.AlterField(
            model_name='thing',
            name='team_name',
            field=models.CharField(unique=True, max_length=255),
        ),
    ]
