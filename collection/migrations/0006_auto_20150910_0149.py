# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('collection', '0005_thing_create_team'),
    ]

    operations = [
        migrations.RenameField(
            model_name='thing',
            old_name='name',
            new_name='team_name',
        ),
        migrations.RemoveField(
            model_name='thing',
            name='create_team',
        ),
    ]
