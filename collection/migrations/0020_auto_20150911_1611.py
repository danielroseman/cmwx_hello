# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('collection', '0019_auto_20150911_1607'),
    ]

    operations = [
        migrations.AlterField(
            model_name='thing',
            name='team',
            field=models.ForeignKey(to='collection.Team', blank=True),
        ),
    ]
