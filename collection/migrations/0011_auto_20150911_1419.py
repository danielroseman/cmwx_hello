# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('collection', '0010_auto_20150911_1407'),
    ]

    operations = [
        migrations.CreateModel(
            name='Team',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=64, unique=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='owner',
            name='teams',
        ),
        migrations.AlterField(
            model_name='thing',
            name='description',
            field=models.TextField(max_length=1024),
        ),
        migrations.AlterField(
            model_name='thing',
            name='team_name',
            field=models.CharField(max_length=64, unique=True),
        ),
        migrations.DeleteModel(
            name='Owner',
        ),
        migrations.AddField(
            model_name='thing',
            name='team',
            field=models.ForeignKey(to='collection.Team', null=True),
        ),
    ]
