# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('collection', '0037_auto_20150912_2133'),
    ]

    operations = [
        migrations.CreateModel(
            name='Game',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('challenger', models.ForeignKey(to='collection.Team', related_name='games_as_challenger')),
                ('creator', models.ForeignKey(to='collection.Person')),
                ('loser', models.ForeignKey(blank=True, related_name='games_as_loser', null=True, to='collection.Team')),
                ('opponent', models.ForeignKey(to='collection.Team', related_name='games_as_opponent')),
                ('winner', models.ForeignKey(blank=True, related_name='games_as_winner', null=True, to='collection.Team')),
            ],
        ),
    ]
