# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('collection', '0008_auto_20150911_1333'),
    ]

    operations = [
        migrations.CreateModel(
            name='Owner',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
            ],
        ),
        migrations.RemoveField(
            model_name='thing',
            name='teams',
        ),
        migrations.AlterField(
            model_name='thing',
            name='user',
            field=models.OneToOneField(to='collection.Owner', blank=True, null=True),
        ),
        migrations.AddField(
            model_name='owner',
            name='teams',
            field=models.ForeignKey(to='collection.Thing', blank=True, null=True),
        ),
    ]
