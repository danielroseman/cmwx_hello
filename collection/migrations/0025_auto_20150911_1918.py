# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('collection', '0024_auto_20150911_1859'),
    ]

    operations = [
        migrations.CreateModel(
            name='Team',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('name', models.CharField(null=True, max_length=64)),
            ],
        ),
        migrations.RemoveField(
            model_name='thing',
            name='team_name',
        ),
        migrations.AddField(
            model_name='thing',
            name='teams',
            field=models.ForeignKey(to='collection.Team', default=1),
            preserve_default=False,
        ),
    ]
