# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('collection', '0013_auto_20150911_1443'),
    ]

    operations = [
        migrations.AlterField(
            model_name='thing',
            name='team',
            field=models.ForeignKey(blank=True, to='collection.Team', null=True),
        ),
    ]
