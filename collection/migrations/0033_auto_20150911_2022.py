# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('collection', '0032_auto_20150911_2013'),
    ]

    operations = [
        migrations.AlterField(
            model_name='team',
            name='name',
            field=models.CharField(unique=True, max_length=64, default=1),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='thing',
            name='teams',
            field=models.ForeignKey(default=1, to='collection.Team'),
            preserve_default=False,
        ),
    ]
