# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('collection', '0030_thing_new_team_name'),
    ]

    operations = [
        migrations.RenameField(
            model_name='thing',
            old_name='teams',
            new_name='team',
        ),
        migrations.RemoveField(
            model_name='thing',
            name='new_team_name',
        ),
        migrations.AddField(
            model_name='thing',
            name='team_name',
            field=models.CharField(default=1, max_length=64, unique=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='team',
            name='name',
            field=models.CharField(null=True, max_length=64, unique=True, blank=True),
        ),
        migrations.AlterField(
            model_name='thing',
            name='user',
            field=models.OneToOneField(blank=True, null=True, to=settings.AUTH_USER_MODEL),
        ),
    ]
