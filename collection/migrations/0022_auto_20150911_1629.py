# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('collection', '0021_auto_20150911_1620'),
    ]

    operations = [
        migrations.AlterField(
            model_name='team',
            name='name',
            field=models.CharField(null=True, max_length=64),
        ),
        migrations.AlterField(
            model_name='thing',
            name='team',
            field=models.ForeignKey(to='collection.Team'),
        ),
    ]
