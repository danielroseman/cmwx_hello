# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def convert_teams(apps, schema_editor):
    Team = apps.get_model("collection", "Team")
    Person = apps.get_model("collection", "Person")

    for person in Person.objects.all():
        team, created = Team.objects.get_or_create(
            name=person.team_name,
            defaults={'slug': person.slug, 'description': person.description}
        )
        person.team = team
        person.save()


class Migration(migrations.Migration):

    dependencies = [
        ('collection', '0035_auto_20150912_2020'),
    ]

    operations = [
        migrations.RunPython(convert_teams)
    ]
