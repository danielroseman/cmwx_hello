# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('collection', '0017_auto_20150911_1538'),
    ]

    operations = [
        migrations.AlterField(
            model_name='team',
            name='name',
            field=models.CharField(max_length=64, null=True, unique=True),
        ),
        migrations.AlterField(
            model_name='thing',
            name='team',
            field=models.ForeignKey(to='collection.Team'),
        ),
    ]
