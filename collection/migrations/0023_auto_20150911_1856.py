# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('collection', '0022_auto_20150911_1629'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='thing',
            name='team',
        ),
        migrations.DeleteModel(
            name='Team',
        ),
    ]
