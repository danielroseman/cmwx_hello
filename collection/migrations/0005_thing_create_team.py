# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('collection', '0004_thing_user'),
    ]

    operations = [
        migrations.AddField(
            model_name='thing',
            name='create_team',
            field=models.CharField(max_length=255, blank=True),
        ),
    ]
