# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('collection', '0007_auto_20150911_1313'),
    ]

    operations = [
        migrations.RenameField(
            model_name='thing',
            old_name='owner',
            new_name='user',
        ),
    ]
