# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('collection', '0014_auto_20150911_1522'),
    ]

    operations = [
        migrations.AlterField(
            model_name='thing',
            name='team',
            field=models.ForeignKey(to='collection.Team', null=True),
        ),
    ]
