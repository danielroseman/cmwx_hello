# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('collection', '0031_auto_20150911_2005'),
    ]

    operations = [
        migrations.RenameField(
            model_name='thing',
            old_name='team',
            new_name='teams',
        ),
    ]
