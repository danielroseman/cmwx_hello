from django.shortcuts import render, redirect
from collection.models import Person, Game, Team
from collection.forms import PersonForm, GameForm
from django.template.defaultfilters import slugify
from django.contrib.auth.decorators import login_required
from django.http import Http404

def home(request):
  persons = Person.objects.all()

  return render(request, 'index.html', {
	   'persons': persons,
  })

def thing_detail(request, slug):
    person = Person.objects.get(user__username=slug)
    context = {'person':person}
    context['game_count'] = person.team.games_as_opponent.count() + person.team.games_as_challenger.count()
    context['win_count'] = person.team.games_as_winner.count()
    context['lose_count'] = person.team.games_as_loser.count()
    return render(request, 'things/thing_detail.html', context)

@login_required
def edit_thing(request, slug):
  person = Person.objects.get(user__username=slug)
  if person.user != request.user:
      raise Http404
  form_class = PersonForm
  if request.method == 'POST':
      form = form_class(data=request.POST, instance=person)
      if form.is_valid():
        form.save()
        return redirect('thing_detail', slug=person.user.username)
  else:
      form = form_class(instance=person, initial={'description': person.team.description})
  return render(request, 'things/edit_thing.html', { 'person': person,'form': form, })

def create_thing(request):
    form_class = PersonForm
    if request.method == 'POST':
        form = form_class(request.POST)
        if form.is_valid():
            person = form.save(commit=False)
            person.user = request.user
            person.save()
            return redirect('thing_detail', slug=person.user.username)
    else:
        form = form_class()
    return render(request, 'things/create_thing.html', {
        'form': form,
    })

def challenge(request):
    person = request.user.person
    if request.method == 'POST':
        form = GameForm(request.POST, person=person)
        if form.is_valid():
            game = form.save(commit=False)
            person = request.user.person
            game.creator = person
            game.challenger = person.team
            game.save()
            return redirect('thing_detail', slug=person.user.username)
    else:
        form = GameForm(person=person)
    return render(request, 'things/challenge.html', {'form': form})

def teams(request):
    return render(request, 'things/teams.html', {'teams': Team.objects.all()})

def results(request):
    games_with_result = Game.objects.exclude(winner=None)
    return render(request, 'things/results.html', {'games': games_with_result})
