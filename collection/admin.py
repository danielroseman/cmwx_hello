from django.contrib import admin

from collection.models import Person, Team, Game
from collection.forms import GameAdminForm

class PersonAdmin(admin.ModelAdmin):
	model = Person
	list_display = ('user', 'team',)

class TeamAdmin(admin.ModelAdmin):
	model = Team
	prepopulated_fields = {'slug': ('name',)}

class GameAdmin(admin.ModelAdmin):
	form = GameAdminForm
	model = Game
	list_display = ('challenger', 'opponent', 'winner')

admin.site.register(Person, PersonAdmin)
admin.site.register(Team, TeamAdmin)
admin.site.register(Game, GameAdmin)
