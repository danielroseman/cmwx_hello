from django.forms import ModelForm
from django import forms
from django.utils.text import slugify
from collection.models import Person, Team, Game

class PersonForm(ModelForm):
    team_name = forms.CharField(max_length=255, required=False)
    description = forms.CharField(widget=forms.Textarea, required=False)

    class Meta:
        model = Person
        fields = ('team',)

    def clean(self):
        team_name = self.cleaned_data.get('team_name')
        team = self.cleaned_data.get('team')

        if not team and not team_name:
            raise forms.ValidationError('You must either select a team or enter a new team name')

        if team and team_name:
            raise forms.ValidationError('You can only enter a team name if you have not selected an existing team')

    def save(self, commit=True):
        obj = super().save(commit=False)
        team_name = self.cleaned_data.get('team_name')
        if team_name:
            team, created = Team.objects.get_or_create(name=team_name)
            team.slug = slugify(team_name)
            obj.team = team
        else:
            team = self.cleaned_data['team']
        team.description = self.cleaned_data.get('description')
        team.save()
        if commit:
            obj.save()
        return obj


class GameForm(ModelForm):
    def __init__(self, *args, **kwargs):
        person = kwargs.pop('person')
        super().__init__(*args, **kwargs)
        self.fields['opponent'].queryset = Team.objects.exclude(id=person.team.id)

    class Meta:
        model = Game
        fields = ('opponent',)


class GameAdminForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance:
            qs = Team.objects.filter(id__in=(self.instance.challenger_id, self.instance.opponent_id))
            self.fields['winner'].queryset = qs

    def save(self, commit=True):
        obj = super().save(commit=False)
        if obj.winner_id:
            if obj.winner_id == obj.challenger_id:
                obj.loser_id = obj.opponent_id
            else:
                obj.loser_id = obj.challenger_id
        if commit:
            obj.save()
        return obj

    class Meta:
        model = Game
        exclude = ('loser',)
